/*
  Copyright © 2016 Hasan Yavuz Özderya

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const Soup = imports.gi.Soup;
const Lang = imports.lang;

// API information
const app_key = "DVgGPFVbdCP41l)aFCSVlQ((";
const app_client_id = "3602";

// API Urls
const api_base = "https://api.stackexchange.com/2.2/";
const api_me = "me";
const api_inbox_unread = "inbox/unread";

function StackApi(logger)
{
    this._init(logger);
}

StackApi.prototype =
{
    _init: function(logger)
    {
        this.l = logger;
        this._token = 0;        // token must be set before any API call

        // setup soup
        this._httpSession = new Soup.Session();
        Soup.Session.prototype.add_feature.call(this._httpSession,
                                                new Soup.ProxyResolverDefault());
    },

    setAccessToken: function (token)
    {
        this._token = token;
    },

    // method: name of the API method (inbox, inbox/unread etc)
    // params: method parameters as a dictionary or `null` if no parameters required
    // callback: called on successfull response
    apiGet: function (method, params, callback)
    {
        try
        {
            if (!this._token)
            {
                this.l.error("Access token is not set!");
                // TODO: why not exception?
                return;
            }

            // setup base url
            let url = api_base + method + "?" +
                "client_id=" + app_client_id +
                "&key=" + app_key +
                "&access_token=" + this._token;

            // add extra parameters
            if (params != null)
            {
                for (var p in params)
                {
                    url += "&" + p + "=" + params[p].toString();
                }
            }
            this.l.log(url);

            // do request
            let message = Soup.Message.new('GET', url);

            this._httpSession.queue_message(
                message,
                Lang.bind(this, this._onApiResponse, callback));
        }
        catch (e)
        {
            this.l.logError(e);
        }
    },

    _onApiResponse: function(session, message, callback)
    {
        try
        {
            this.l.log("response status code:" + message.status_code.toString());
            if (message.status_code == Soup.KnownStatusCode.OK)
            {
                let r = JSON.parse(message.response_body.data);
                this.l.log("Remaining quota: " + r.quota_remaining.toString());
                callback.call(this, r);
            }
            else
            {
                // TODO: notify the api error
                this.l.error("API get failed. Response body:");
                this.l.error(message.response_body.data);
            }
        }
        catch (e)
        {
            this.l.error(e);
        }
    }
}
