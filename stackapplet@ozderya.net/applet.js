/*
  Copyright © 2016 Hasan Yavuz Özderya

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const St = imports.gi.St;
const GLib = imports.gi.GLib;
const Secret = imports.gi.Secret;
const PopupMenu = imports.ui.popupMenu;
const Applet = imports.ui.applet;
const Settings = imports.ui.settings;
const Mainloop = imports.mainloop;
const Util = imports.misc.util;
const Gettext = imports.gettext.domain('cinnamon-applets');
const _ = Gettext.gettext;

// for debugging purposes only
function dump(x)
{
    if (typeof x === "object" && x !== null) {
        global.log(JSON.stringify(x));
    }
    else
    {
        global.log(String(x));
    }
}

// update period for loading unread itmes
const UPDATE_PERIOD = 5 * 60 * 1000; // 5 minutes in milliseconds
// const UPDATE_PERIOD = 1000; // 1 second in milliseconds

// libscret Schema for storing access token
const ACCESSTOKEN_SCHEMA = new Secret.Schema(
    "org.stackapplet.AccessToken",
    Secret.SchemaFlags.NONE,
    {
        "instance_id": Secret.SchemaAttributeType.INTEGER,
    }
);

function Logger(uuid) {
    this._init(uuid);
}

Logger.prototype = {

    _init: function (uuid)
    {
        this.uuid = uuid;
    },

    log: function(s)
    {
        global.log(this.uuid + ': ' + s.toString());
    },

    error: function(s)
    {
        global.logError(this.uuid + ': ' + s.toString());
    }
}

function MyApplet(metadata, orientation, panel_height, instance_id)
{
    this._init(metadata, orientation, panel_height, instance_id);
}

// item: an `inbox_item` object returned from api
function MenuInboxItem(item) {
    this._init(item)
}

MenuInboxItem.prototype = {
    __proto__: PopupMenu.PopupBaseMenuItem.prototype,

    _init: function (item)
    {
        PopupMenu.PopupBaseMenuItem.prototype._init.call(this);

        // Important: we use 'link' field to uniquely identify an item. That's
        // because an inbox item can refer to different things such as
        // questions, comments, answers etc and those all have different ID
        // fields. Some doesn't even have IDs (chat messages). But all of them
        // seem to have a 'link' field which we assume is unique.
        //
        // For more info: https://api.stackexchange.com/docs/types/inbox-item

        this.inbox_item = item;

        let titleLabel = new St.Label(
            {text: item.title, style_class: "stackapplet-title-label"});
        let whatWhereLabel = new St.Label(
            {text: this._whatWhere(item), style_class: "stackapplet-whatwhere-label"});

        let removeIcon = new St.Icon({
            icon_name:   'edit-delete',
            icon_type:   St.IconType.SYMBOLIC,
            style_class: 'popup-menu-icon'
        });
        let removeButton = new St.Button(
            {child: removeIcon, style_class: "stackapplet-remove-button"});

        // setup layout
        let vbox = new St.BoxLayout({vertical: true});
        vbox.add(titleLabel);
        vbox.add(whatWhereLabel);
        this.addActor(vbox, {expand: true});
        this.addActor(removeButton, {expand: false, align: St.Align.END});

        // setup signals
        removeButton.connect('clicked',
                             Lang.bind(this,
                                       function()
                                       {
                                           this.emit("remove-clicked", this.inbox_item);
                                       }));

        this.connect('activate', Lang.bind(this, this._visit));
    },

    _visit: function ()
    {
        Util.spawnCommandLine("xdg-open " + this.inbox_item.link);
    },

    // from `item` returns a text like 'comment on StackOverflow'
    _whatWhere: function (item)
    {
        let map =
            {
                comment             : "new comment",
                chat_message        : "new chat message",
                new_answer          : "new new answer",
                careers_message     : "new career message",
                careers_invitations : "new career invitation",
                meta_question       : "new meta question",
                post_notice         : "new notice",
                moderator_message   : "new moderator message"
            };

        let type = "";
        if (map[item.item_type])
        {
            type = map[item.item_type];
        }
        else
        {
            type = "new item";
        }

        return type + " on " + item.site.name;
    }
}

MyApplet.prototype = {
    __proto__: Applet.TextApplet.prototype,

    _init: function(metadata, orientation, panel_height, instance_id)
    {
        Applet.TextApplet.prototype._init.call(this, orientation);

        this.metadata = metadata;
        this.auth_in_progress = false;
        this.instance_id = instance_id;

        try
        {
            this.l = new Logger(metadata['uuid']);  // init logger

            this.menuManager = new PopupMenu.PopupMenuManager(this);
            this.menu = new Applet.AppletPopupMenu(this, orientation);
            this.menuManager.addMenu(this.menu);

            this.menu.addMenuItem(
                new PopupMenu.PopupMenuItem(_("You have no messages!")));

            // init settings
            this.settings = new Settings.AppletSettings(
                this, metadata['uuid'], instance_id);
            this.settings.bindProperty(Settings.BindingDirection.OUT,
                'is_logged_in', 'is_logged_in');
            this.settings.bindProperty(Settings.BindingDirection.OUT,
                'oldest_item_time', 'oldest_item_time');
            this.settings.bindProperty(Settings.BindingDirection.OUT,
                'removed_items', 'removed_items');

            // debug code
            global.log(this.removed_items);

            // init API
            imports.searchPath.push(metadata.path);
            this.api = new imports.stackapi.StackApi(this.l);

            if (this.is_logged_in)
            {
                this.l.log("Authorized.");
                this.set_applet_tooltip(_("Loading..."));
                this.set_applet_label("S[…]");
                this._loadToken(); // will call `_start`
            }
            else
            {
                this.l.log("Not authorized.");
                this.set_applet_tooltip(_("Click to login!"));
                this.set_applet_label("S[!]");
            }
        }
        catch (e)
        {
            this.l.error(e);
        }
    },

    // Launches the authorization window
    start_authorization: function()
    {
        try
        {
            this.l.log("Starting authorization.");

            let auth_script = this.metadata.path + '/authorize.js';
            Util.spawn_async(['cjs', auth_script],
                             Lang.bind(this, this._on_authorize_ended));

            this.auth_in_progress = true;
        }
        catch (e)
        {
            this.l.error(e);
        }
    },

    // Called when authorization script is ended
    _on_authorize_ended: function(results)
    {
        try
        {
            results = JSON.parse(results);

            if (results.success)
            {
                let access_token = results.access_token;
                this.l.log("Authorization is successful.");
                this._storeToken(access_token)
            }
            else
            {
                this.l.error("Authorization failed. Error: " + results.error);
            }

            this.auth_in_progress = false;
        }
        catch (e)
        {
            this.l.error(e);
        }
    },

    _storeToken: function (token)
    {
        this.l.log("Storing token...");
        Secret.password_store(ACCESSTOKEN_SCHEMA,
                              {"instance_id" : this.instance_id},
                              Secret.COLLECTION_DEFAULT,
                              "StackApplet API access token",
                              token,
                              null,
                              Lang.bind(this, this._onTokenStored, token));
    },

    _onTokenStored: function (source, result, token)
    {
        try
        {
            this.l.log("Token stored.");
            Secret.password_store_finish(result);
            this.is_logged_in = true;
            this.api.setAccessToken(token);
            this._start();
        }
        catch (e)
        {
            this.l.error(e);
        }
    },

    // Loads the access token from the password store
    _loadToken: function ()
    {
        Secret.password_lookup(ACCESSTOKEN_SCHEMA,
                               {"instance_id" : this.instance_id},
                               null,
                               Lang.bind(this, this._onLoadToken));
    },

    // Called after access token is loaded from the password store, triggers `_start`
    _onLoadToken: function (source, result)
    {
        try
        {
            let access_token = Secret.password_lookup_finish(result);
            if (access_token)
            {
                this.l.log("Loaded access token.");
                this.api.setAccessToken(access_token);
                this._start();
            }
            else
            {
                this.l.error("Failed to load access token from the password store.");
                // TODO: error case, invalidate access token?
            }
        }
        catch (e)
        {
            this.l.error(e);
        }
    },

    // should be called after api access token is set
    _start: function()
    {
        this._loadUnread();
        this._startUpdate();
    },

    // start the update timer
    _startUpdate: function()
    {
        this._updateTimer = Mainloop.timeout_add(
            UPDATE_PERIOD, Lang.bind(this, function()
                                     {
                                         this._loadUnread();
                                         return true;
                                     }));
    },

    // stop the update timer if its running
    _stopUpdate: function()
    {
        try
        {
            if (this._updateTimer)
            {
                Mainloop.source_remove(this._updateTimer);
                this._updateTimer = undefined;
            }
        }
        catch (e)
        {
            global.logError(e);
        }
    },

    // load unread items
    _loadUnread: function()
    {
        // TODO: for testing purposes we are loading 'inbox', change to 'inbox/unread'
        try
        {
            this.l.log("Loading inbox...");

            // NOTE: `inbox` doesn't support the `since` argument, only
            // `inbox/unread` does
            this.api.apiGet('inbox',
                            {pagesize:10, since:this.oldest_item_time},
                            Lang.bind(this, this._onLoadUnread));
        }
        catch (e)
        {
            global.logError(e);
        }
    },

    _onLoadUnread: function(result)
    {
        try
        {
            let num = result.items.length;
            this.l.log(_("Loaded %d items.").format(num));

            this._filterItems(result.items);

            num = this._loaded_items.length; // count after filtering
            this.l.log(_("Left %d items after filtering.").format(num));

            if (num)
            {
                this.set_applet_tooltip(_("%d unread items").format(num));
                this._showUnread(this._loaded_items)
            }
            else
            {
                this.set_applet_tooltip(_("No unread items"));
            }
            this.set_applet_label(_("S[%d]").format(num));
        }
        catch (e)
        {
            global.logError(e);
        }
    },

    /// Filters the loaded unread items. Results are returned and stored in
    /// `this._loaded_items`.
    _filterItems: function (items)
    {
        // first update the 'removed_items', get rid of the ones that are
        // not in the loaded list anymore
        this._loaded_items = items;
        this.removed_items = this.removed_items.filter(
            function(ritem_link)
            {
                for each (var litem in this._loaded_items)
                {
                    if (litem.link == ritem_link) return true;
                }
                return false;
                // return this._loaded_items.indexOf(item.link) != -1;
            }, this);

        // now filter loaded items
        this._loaded_items = this._loaded_items.filter(
            function(item)
            {
                // check that if item is not removed
                return this.removed_items.indexOf(item.link) < 0;
            }, this);

        // update oldest item time
        let oldest_time = this._loaded_items[0].creation_date;
        for each (var litem in this._loaded_items)
        {
            if (litem.creation_date < oldest_time)
            {
                oldest_time = litem.creation_date;
            }
        }
        this.oldest_item_time = oldest_time;

        return this._loaded_items;
    },

    // updates the menu with the list of unread items
    // items: should be the 'items' member from the API result
    _showUnread: function(items)
    {
        try
        {
            this.menu.removeAll();
            for each (var item in items)
            {
                let mitem = new MenuInboxItem(item);
                // TODO: use the signal manager
                mitem.connect("remove-clicked", Lang.bind(this, this._onRemoveClicked));
                this.menu.addMenuItem(mitem);
            }
        }
        catch (e)
        {
            global.logError(e);
        }
    },

    _onRemoveClicked: function (source, item)
    {
        // switch the focus to `menu` from `menuItem` to prevent menu from closing
        this.menu.actor.grab_key_focus();

        source.destroy();       // remove menu item

        // This little temp variable trick is required because `push` method
        // doesn't trigger a save for some reason.
        let temp_items = this.removed_items;
        temp_items.push(item.link);
        this.removed_items = temp_items;

        // TODO: we will also have to store removed items time
        // TODO: update oldest time

        global.log(item.link);
    },

    on_applet_clicked: function(event)
    {
        this.l.log("applet clicked");

        if (!this.is_logged_in)
        {
            if (!this.auth_in_progress) this.start_authorization();
        }
        else
        {
            this.menu.toggle();
        }
    },

    on_applet_removed_from_panel: function()
    {
        // TODO: logout, remove access token
        this._stopUpdate();
    }
};

function main(metadata, orientation, panel_height, instance_id) {
    return new MyApplet(metadata, orientation, panel_height, instance_id);
}
