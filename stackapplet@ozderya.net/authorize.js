#!/usr/bin/cjs
/*
  Copyright © 2016 Hasan Yavuz Özderya

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*

  Authorization script for StackApplet

  Results are printed in JSON format. Example successfull result:

  {
    "success" : true,
    "access_token" : "XXXXXX"
  }

  Example of an error:

  {
    "success" : false,
    "error" : "Error string"
  }

*/

const Gtk = imports.gi.Gtk;
const WebKit = imports.gi.WebKit2;
const Lang = imports.lang;

// StackApplet authentication keys
const app_key = "DVgGPFVbdCP41l)aFCSVlQ((";
const app_client_id = "3602";

// authorization information for stackexchange API
const auth_url = "https://stackexchange.com/oauth/dialog";
const auth_redirect_url = "https://stackexchange.com/oauth/login_success";

const WebBrowser = new Lang.Class({
    Name: 'WebBrowser',
    Extends: Gtk.Application,

    // create the application
    _init: function()
    {
        this.parent({application_id: 'org.stackapplet.WebBrowser'});

        this.connect('activate', Lang.bind(this, this._onActivate));
        this.connect('startup', Lang.bind(this, this._onStartup));
    },

    _onActivate: function()
    {
        this._window.present();

        // navigate to authorization URL
        let url = auth_url +
            "?client_id=" + app_client_id +
            "&key=" + app_key +
            "&scope=read_inbox,no_expiry" +
            "&redirect_uri=" + auth_redirect_url;

        this._webView.load_uri(url);
    },

    _onStartup: function()
    {
        this._buildUI();
        this._connectSignals();
    },

    _buildUI: function()
    {
        // setup the window
        this._window = new Gtk.ApplicationWindow({
            application     : this,
            window_position : Gtk.WindowPosition.CENTER,
            default_width   : 900,
            default_height  : 800,
            border_width    : 0,
            title           : "StackApplet Authorization"
        });

        // setup the webview widget
        this._webView = new WebKit.WebView();

        let scrolledWindow = new Gtk.ScrolledWindow({
            hscrollbar_policy: Gtk.PolicyType.AUTOMATIC,
            vscrollbar_policy: Gtk.PolicyType.AUTOMATIC
        });
        scrolledWindow.add(this._webView);

        this._window.add(scrolledWindow);

        this._window.show_all();
    },

    _connectSignals: function()
    {
        this._webView.connect('load-changed',
                              Lang.bind(this, this._onLoadChanged));

        this._window.connect('destroy',
                             Lang.bind(this, this._onClose));
    },

    _onLoadChanged: function(source, event)
    {
        let url = this._webView.get_uri();

        // check if we are redirected to success URL
        if (url.search(auth_redirect_url) ==0 ) {
            this._authSuccess(url);
        }

        // TODO: error cases
    },

    _onClose: function(source, event)
    {
        let results = {'success': false,
                       'error' : "User canceled authorization."};
        print(JSON.stringify(results));
    },

    // Will be called when we are redirected to given "redirection URL"
    _authSuccess: function (url)
    {
        let results = {'success': false};

        // get access token from the url
        try
        {
            let _access_token =
                /access_token=[^&]+/.exec(url).toString().replace("access_token=","");

            // results
            results.success = true;
            results.access_token = _access_token;
        }
        catch (err)
        {
            results.success = false;
            results.error = "Authentication key could not be extracted from the url:" + url;
        }

        print(JSON.stringify(results));
        this.quit(); // quit authentication window
    }

});

// run application
let app = new WebBrowser();
app.run(ARGV);
